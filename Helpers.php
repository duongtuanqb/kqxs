<?php 
namespace App;

use DiDom\Document;
use DiDom\Query;

define("LICH_XO_SO", [
  [ "Đà Lạt","Khánh Hòa","Kiên Giang","Kon Tum","Tiền Giang","Thái Bình" ],
  [ "Cà Mau","Đồng Tháp","Phú Yên","Thừa Thiên Huế","TP. HCM","Hà Nội" ],
  [ "Bạc Liêu","Bến Tre","Đắk Lắk","Quảng Nam","Vũng Tàu","Quảng Ninh" ],
  [ "Cần Thơ","Đà Nẵng","Đồng Nai","Khánh Hòa","Sóc Trăng","Bắc Ninh" ],
  [ "An Giang", "Bình Định", "Bình Thuận", "Quảng Bình", "Quảng Trị", "Tây Ninh", "Hà Nội" ],
  [ "Bình Dương", "Gia Lai", "Ninh Thuận", "Trà Vinh", "Vĩnh Long", "Hải Phòng" ],
  [ "Bình Phước", "Đà Nẵng", "Đắk Nông", "Hậu Giang", "Long An", "Quảng Ngãi", "TP. HCM", "Nam Định" ]
]);

class Helpers {

  public function getKQXS($url) {
      $ketQua = [];

      $document = new Document($this->curl($url));

      if (count($box_kqxs = $document->find('.box_kqxs>table'))) {
          $rows = $box_kqxs[0]->find("tbody>tr");

          foreach ($rows as $key => $row) {
              $tenGiai = trim($row->find("td")[1]->class);
              foreach ($row->find("td")[1]->find(".dayso") as $dayso) {
                  $ketQua[$tenGiai][] = $dayso->text();
              }
          }

          return json_encode($ketQua);
      }
  }

  public function checkNgay($box_kqxs, $url) {
      preg_match("/.+\/([^?]+).html/", $url, $dateURL);

      $date = $box_kqxs->find(".daymonth")[0]->text()."/".$box_kqxs->find(".year")[0]->text();
      return \DateTime::createFromFormat('d/m/Y', $date)->format("d-m-Y") == $dateURL[1];
  }

  public function checkLich($tinh, $ngay) {
      $thu = $this->getThu($ngay);

      foreach (LICH_XO_SO[$thu] as $lich) {
          if($this->convert_string_vi($tinh) == $this->convert_string_vi($lich)) {
              return true;
          }
      }

      return false;
  }

  public function curl($url) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36');
      $result = curl_exec($ch);
      curl_close($ch);
      return $result;
  }

  public function getThu($date) {
    $date_format = date_format(date_create($date), "Y-m-d");
    return date('w', strtotime($date_format));
  }

  public function convert_string_vi($string) {
      if (!$string) return false;
      $utf8 = array(
          'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
          'd' => 'đ|Đ',
          'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
          'i' => 'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',
          'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
          'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
          'y' => 'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
      );
      foreach ($utf8 as $ascii => $uni) $string = preg_replace("/($uni)/i", $ascii, $string);
      $string = $this->convert_utf8($string);
      $string = trim($string, '-');
      return $string;
  }

  public function convert_utf8($string) {
      $string = strtolower($string);
      $string = str_replace("ß", "ss", $string);
      $string = str_replace("%", "", $string);
      $string = preg_replace("/[^_a-zA-Z0-9 -]/", "", $string);
      $string = str_replace(array('%20', ' '), '-', $string);
      $string = str_replace("----", "-", $string);
      $string = str_replace("---", "-", $string);
      $string = str_replace("--", "-", $string);
      return $string;
  }

  public function json_response($message = null, $code = 200) {
      header_remove();
      http_response_code($code);

      header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
      header('Content-Type: application/json');

      $status = array(
          200 => '200 OK',
          400 => '400 Bad Request',
          404 => '404 Not Found',
          422 => 'Unprocessable Entity',
          500 => '500 Internal Server Error'
          );

      header('Status: '.$status[$code]);

      return json_encode(array(
          'status_code' => $code, // success or not?
          'data' => [
            'message' => $message
          ]
          ));
  }

}