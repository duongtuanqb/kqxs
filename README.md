# API lấy kết quả xổ số
Trang lấy: http://lechung.net/

## Installation

Đổi `.env.example` sang `.env`. 
Sửa file .env để thay đôi password API

```bash
$ cp .env.example .env
```

Cài đặt thư viện 
```bash
$ composer install
```

## Tests

```bash
$ ./vendor/bin/phpunit
```

## Run

```curl
$ curl ./index.php?tinh=<tên tỉnh>&ngay=<d-m-Y>&pass=<mật khẩu>
```
## Danh sách tỉnh thành có kết quả xổ số
Thay `Name` hoặc `Slug` vào `<tên tỉnh>` trong url

| Name | Slug |
| ---- | ---- |
| Đà Lạt | da-lat |
| Khánh Hòa | khanh-hoa | 
| Kiên Giang | kien-giang | 
| Kon Tum | kon-tum | 
| Tiền Giang | tien-giang |
| Thái Bình | thai-binh |
| Cà Mau | ca-mau |
| Đồng Tháp | dong-thap |
| Phú Yên | phu-yen |
| Thừa Thiên Huế | thu-thien-hue |
| TP. HCM | tp-hcm |
| Hà Nội | ha-noi |
| Bạc Liêu | bac-lieu |
| Bến Tre | ben-tre |
| Đắk Lắk | dak-lak |
| Quảng Nam | quang-nam |
| Vũng Tàu | vung-tau |
| Quảng Ninh | quang-ninh |
| Cần Thơ | can-tho |
| Đà Nẵng | da-nang |
| Đồng Nai | dong-nai |
| Sóc Trăng | soc-trang | 
| Bắc Ninh | bac-ninh |
| An Giang | an-giang |
| Bình Định | binh-dinh |
| Bình Thuận | binh-thuan |
| Quảng Bình | quang-binh |
| Quảng Trị | quang-tri |
| Tây Ninh | tay-ninh |
| Bình Dương | binh-duong | 
| Gia Lai | gia-lai |
| Ninh Thuận | ninh-thua |
| Trà Vinh | tra-vinh |
| Vĩnh Long | vinh-long |
| Hải Phòng | hai-phong |
| Bình Phước | binh-phuoc |
| Đắk Nông | dak-nong |
| Hậu Giang | hau-giang |
| Long An | long-an |
| Quảng Ngãi | quang-ngai |
| Nam Định | nam-dinh |