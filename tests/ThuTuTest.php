<?php
/**
 * Test Xo so thu tu ngay 01-08-2018
 * [ "Cần Thơ","Đà Nẵng","Đồng Nai","Khánh Hòa","Sóc Trăng","Bắc Ninh" ]
 */
namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Helpers;

class ThuTuTests extends TestCase
{
	/**
     * @dataProvider providerXoSoNgayThuTu
     */
    
	public function testXoSoNgayThuTu($tinh, $expectedResult)
	{
		$helpers = new Helpers();

	    $ketquaXS = $helpers->getKQXS("http://lechung.net/ket-qua-xo-so-" . $helpers->convert_string_vi($tinh) . "/01-08-2018.html");
		$ketquaXS = json_decode($ketquaXS, true);

		if(isset($ketquaXS["giai_tam"])) {
		    $this->assertEquals($ketquaXS["giai_tam"], $expectedResult["giai_tam"]);
		}

	    $this->assertEquals($ketquaXS["giai_bay"], $expectedResult["giai_bay"]);
	    $this->assertEquals($ketquaXS["giai_sau"], $expectedResult["giai_sau"]);
	    $this->assertEquals($ketquaXS["giai_nam"], $expectedResult["giai_nam"]);
	    $this->assertEquals($ketquaXS["giai_tu"], $expectedResult["giai_tu"]);
	    $this->assertEquals($ketquaXS["giai_ba"], $expectedResult["giai_ba"]);
	    $this->assertEquals($ketquaXS["giai_nhi"], $expectedResult["giai_nhi"]);
	    $this->assertEquals($ketquaXS["giai_nhat"], $expectedResult["giai_nhat"]);
	    $this->assertEquals($ketquaXS["giai_dac_biet"], $expectedResult["giai_dac_biet"]);
	}

	public function providerXoSoNgayThuTu() {
	    return [
	        	[
		        	"Cần Thơ", [
		        		"giai_tam" => ["26"],
						"giai_bay" => ["492"],
						"giai_sau" => ["8091","2974","9981"],
						"giai_nam" => ["4299"],
						"giai_tu" => ["52740","10771","63586","58791","05631","59429","36545"],
						"giai_ba" => ["02228","18607"],
						"giai_nhi" => ["18634"],
						"giai_nhat" => ["69467"],
						"giai_dac_biet" => ["392231"]
		        	]
		        ],

	        	[
	        		"Đà Nẵng", [
		        		"giai_tam" => ["24"],
						"giai_bay" => ["653"],
						"giai_sau" => ["0014","8331","3371"],
						"giai_nam" => ["7619"],
						"giai_tu" => ["55429","69100","87021","55973","09893","14561","30803"],
						"giai_ba" => ["90286","05739"],
						"giai_nhi" => ["35014"],
						"giai_nhat" => ["74920"],
						"giai_dac_biet" => ["741741"]
		        	],
	        	],

	        	[
	        		"Đồng Nai", [
	        			"giai_tam" => ["00"],
						"giai_bay" => ["350"],
						"giai_sau" => ["9443","3669","6323"],
						"giai_nam" => ["1877"],
						"giai_tu" => ["06935","34347","74686","50437","37541","47691","71929"],
						"giai_ba" => ["33367","34348"],
						"giai_nhi" => ["84670"],
						"giai_nhat" => ["05027"],
						"giai_dac_biet" => ["261679"]
	        		]
	        	],

	        	[
	        		"Khánh Hòa", [
						"giai_tam" => ["91"],
						"giai_bay" => ["378"],
						"giai_sau" => ["6397","7855","0827"],
						"giai_nam" => ["4352"],
						"giai_tu" => ["46075","54852","46662","22716","68789","25112","83020"],
						"giai_ba" => ["87672","53698"],
						"giai_nhi" => ["46513"],
						"giai_nhat" => ["44372"],
						"giai_dac_biet" => ["878472"]
	        		]
	        	],

	        	[
	        		"Sóc Trăng", [
	        			"giai_tam" => ["38"],
						"giai_bay" => ["697"],
						"giai_sau" => ["0604","1976","3225"],
						"giai_nam" => ["9322"],
						"giai_tu" => ["03983","09049","11970","28582","15063","87495","29626"],
						"giai_ba" => ["84485","78941"],
						"giai_nhi" => ["62394"],
						"giai_nhat" => ["80955"],
						"giai_dac_biet" => ["398216"]
	        		]
	        	],

	        	[
	        		"Bắc Ninh", [
						"giai_bay" => ["47","14","11","49"],
						"giai_sau" => ["163","419","221"],
						"giai_nam" => ["8910","8864","5787","7967","4007","2167"],
						"giai_tu" => ["7067","5799","2969","0420"],
						"giai_ba" => ["11509","16747","73342","27766","09650","33345"],
						"giai_nhi" => ["04755","58311"],
						"giai_nhat" => ["84415"],
						"giai_dac_biet" => ["41128"]
	        		]
	        	]
	    ];
	}	
}