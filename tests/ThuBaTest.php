<?php
/**
 * Test Xo so thu ba ngay 31-07-2018
 * [ "Bạc Liêu","Bến Tre","Đắk Lắk","Quảng Nam","Vũng Tàu","Quảng Ninh" ]
 */
namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Helpers;

class ThuBaTests extends TestCase
{
	/**
     * @dataProvider providerXoSoNgayThuBa
     */
    
	public function testXoSoNgayThuBa($tinh, $expectedResult)
	{
		$helpers = new Helpers();

	    $ketquaXS = $helpers->getKQXS("http://lechung.net/ket-qua-xo-so-" . $helpers->convert_string_vi($tinh) . "/31-07-2018.html");
		$ketquaXS = json_decode($ketquaXS, true);

		if(isset($ketquaXS["giai_tam"])) {
		    $this->assertEquals($ketquaXS["giai_tam"], $expectedResult["giai_tam"]);
		}

	    $this->assertEquals($ketquaXS["giai_bay"], $expectedResult["giai_bay"]);
	    $this->assertEquals($ketquaXS["giai_sau"], $expectedResult["giai_sau"]);
	    $this->assertEquals($ketquaXS["giai_nam"], $expectedResult["giai_nam"]);
	    $this->assertEquals($ketquaXS["giai_tu"], $expectedResult["giai_tu"]);
	    $this->assertEquals($ketquaXS["giai_ba"], $expectedResult["giai_ba"]);
	    $this->assertEquals($ketquaXS["giai_nhi"], $expectedResult["giai_nhi"]);
	    $this->assertEquals($ketquaXS["giai_nhat"], $expectedResult["giai_nhat"]);
	    $this->assertEquals($ketquaXS["giai_dac_biet"], $expectedResult["giai_dac_biet"]);
	}

	public function providerXoSoNgayThuBa() {
	    return [
	        	[
		        	"Bạc Liêu", [
		        		"giai_tam" => ["24"],
						"giai_bay" => ["224"],
						"giai_sau" => ["6963","7813","7111"],
						"giai_nam" => ["7563"],
						"giai_tu" => ["05373","67798","30326","80423","62748","89300","49537"],
						"giai_ba" => ["72792","27950"],
						"giai_nhi" => ["12169"],
						"giai_nhat" => ["25115"],
						"giai_dac_biet" => ["800631"]
		        	]
		        ],

	        	[
	        		"Bến Tre", [
		        		"giai_tam" => ["22"],
						"giai_bay" => ["880"],
						"giai_sau" => ["1410","1971","6142"],
						"giai_nam" => ["5439"],
						"giai_tu" => ["79214","12026","13439","02320","86906","59203","51301"],
						"giai_ba" => ["03596","21708"],
						"giai_nhi" => ["82903"],
						"giai_nhat" => ["91602"],
						"giai_dac_biet" => ["999039"]
		        	],
	        	],

	        	[
	        		"Đắk Lắk", [
	        			"giai_tam" => ["86"],
						"giai_bay" => ["433"],
						"giai_sau" => ["9821","8714","4069"],
						"giai_nam" => ["6355"],
						"giai_tu" => ["16595","40199","51989","00964","17600","53603","55599"],
						"giai_ba" => ["61985","14877"],
						"giai_nhi" => ["21284"],
						"giai_nhat" => ["97106"],
						"giai_dac_biet" => ["325220"]
	        		]
	        	],

	        	[
	        		"Quảng Nam", [
						"giai_tam" => ["55"],
						"giai_bay" => ["568"],
						"giai_sau" => ["4930","0274","9790"],
						"giai_nam" => ["1899"],
						"giai_tu" => ["39012","62944","16602","76884","24500","59867","63130"],
						"giai_ba" => ["65299","77451"],
						"giai_nhi" => ["92985"],
						"giai_nhat" => ["15273"],
						"giai_dac_biet" => ["731769"]
	        		]
	        	],

	        	[
	        		"Vũng Tàu", [
	        			"giai_tam" => ["04"],
						"giai_bay" => ["646"],
						"giai_sau" => ["5108","1660","5766"],
						"giai_nam" => ["8751"],
						"giai_tu" => ["43321","05561","76729","10864","66958","87945","36302"],
						"giai_ba" => ["55093","50934"],
						"giai_nhi" => ["89891"],
						"giai_nhat" => ["01324"],
						"giai_dac_biet" => ["199813"]
	        		]
	        	],

	        	[
	        		"Quảng Ninh", [
						"giai_bay" => ["70","86","48","51"],
						"giai_sau" => ["139","242","061"],
						"giai_nam" => ["7274","3516","7737","4944","0777","6783"],
						"giai_tu" => ["0301","8076","7202","5826"],
						"giai_ba" => ["15446","84232","72103","25666","46392","67876"],
						"giai_nhi" => ["36893","89823"],
						"giai_nhat" => ["77456"],
						"giai_dac_biet" => ["28236"]
	        		]
	        	]
	    ];
	}	
}