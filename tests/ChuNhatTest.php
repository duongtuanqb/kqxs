<?php
/**
 * Test Xo so chu nhat ngay 29-07-2018
 * [ "Đà Lạt","Khánh Hòa","Kiên Giang","Kon Tum","Tiền Giang","Thái Bình" ]
 */
namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Helpers;

define("ENDPOINT", "http://lechung.net/");

class ChuNhatTests extends TestCase
{
	/**
     * @dataProvider providerXoSoNgayChuNhat
     */
    
	public function testXoSoNgayChuNhat($tinh, $expectedResult)
	{
		$helpers = new Helpers();

	    $ketquaXS = $helpers->getKQXS("http://lechung.net/ket-qua-xo-so-" . $helpers->convert_string_vi($tinh) . "/29-07-2018.html");
		$ketquaXS = json_decode($ketquaXS, true);

		if(isset($ketquaXS["giai_tam"])) {
		    $this->assertEquals($ketquaXS["giai_tam"], $expectedResult["giai_tam"]);
		}

	    $this->assertEquals($ketquaXS["giai_bay"], $expectedResult["giai_bay"]);
	    $this->assertEquals($ketquaXS["giai_sau"], $expectedResult["giai_sau"]);
	    $this->assertEquals($ketquaXS["giai_nam"], $expectedResult["giai_nam"]);
	    $this->assertEquals($ketquaXS["giai_tu"], $expectedResult["giai_tu"]);
	    $this->assertEquals($ketquaXS["giai_ba"], $expectedResult["giai_ba"]);
	    $this->assertEquals($ketquaXS["giai_nhi"], $expectedResult["giai_nhi"]);
	    $this->assertEquals($ketquaXS["giai_nhat"], $expectedResult["giai_nhat"]);
	    $this->assertEquals($ketquaXS["giai_dac_biet"], $expectedResult["giai_dac_biet"]);
	}

	public function providerXoSoNgayChuNhat() {
	    return [
	        	[
		        	"Đà Lạt", [
		        		"giai_tam" => ["92"],
		        		"giai_bay" => ["552"],
		        		"giai_sau" => ["6952", "9847", "1490"],
		        		"giai_nam" => ["6472"],
		        		"giai_tu" => ["69527", "80145", "83445", "30038", "01149", "78952", "13799"],
		        		"giai_ba" => ["65441", "01761"],
		        		"giai_nhi" => ["62612"],
		        		"giai_nhat" => ["74459"],
		        		"giai_dac_biet" => ["553971"]
		        	]
		        ],

	        	[
	        		"Khánh Hòa", [
		        		"giai_tam" => ["52"],
		        		"giai_bay" => ["123"],
		        		"giai_sau" => ["0759", "3583", "0437"],
		        		"giai_nam" => ["3115"],
		        		"giai_tu" => ["20784", "86299", "11641", "99052", "56061", "21403", "00885"],
		        		"giai_ba" => ["39812", "78426"],
		        		"giai_nhi" => ["58933"],
		        		"giai_nhat" => ["88291"],
		        		"giai_dac_biet" => ["533377"]
		        	],
	        	],

	        	[
	        		"Kiên Giang", [
	        			"giai_tam" => ["00"],
		        		"giai_bay" => ["777"],
		        		"giai_sau" => ["2464", "3553", "7374"],
		        		"giai_nam" => ["6319"],
		        		"giai_tu" => ["63285", "72373", "93023", "76743", "17053", "52218", "24276"],
		        		"giai_ba" => ["13342", "68805"],
		        		"giai_nhi" => ["65078"],
		        		"giai_nhat" => ["72005"],
		        		"giai_dac_biet" => ["993654"]
	        		]
	        	],

	        	[
	        		"Kon Tum", [
	        			"giai_tam" => ["29"],
		        		"giai_bay" => ["331"],
		        		"giai_sau" => ["6586","0607","8113"],
		        		"giai_nam" => ["6074"],
		        		"giai_tu" => ["39542","36332","68240","57190","27080","68055","82269"],
		        		"giai_ba" => ["07237","67047"],
		        		"giai_nhi" => ["81259"],
		        		"giai_nhat" => ["03753"],
		        		"giai_dac_biet" => ["368878"]
	        		]
	        	],

	        	[
	        		"Tiền Giang", [
	        			"giai_tam" => ["79"],
						"giai_bay" => ["863"],
						"giai_sau" => ["0766","5509","2465"],
						"giai_nam" => ["0452"],
						"giai_tu" => ["51895","43679","89741","80088","35952","51625","53760"],
						"giai_ba" => ["14959","72120"],
						"giai_nhi" => ["78375"],
						"giai_nhat" => ["06916"],
						"giai_dac_biet" => ["979393"]
	        		]
	        	],

	        	[
	        		"Thái Bình", [
						"giai_bay" => ["47","01","48","05"],
						"giai_sau" => ["680","759","492"],
						"giai_nam" => ["2139","6041","0231","3959","8693","1839"],
						"giai_tu" => ["8495","5161","6192","5157"],
						"giai_ba" => ["03588","36484","42466","42433","87586","94282"],
						"giai_nhi" => ["28702","13720"],
						"giai_nhat" => ["88817"],
						"giai_dac_biet" => ["69492"]
	        		]
	        	]
	    ];
	}	
}