<?php
/**
 * Test Xo so thu bay ngay 04-08-2018
 * [ "Bình Phước", "Đà Nẵng", "Đắk Nông", "Hậu Giang", "Long An", "Quảng Ngãi", "TP. HCM", "Nam Định" ]
 */
namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Helpers;

class ThuBayTests extends TestCase
{
	/**
     * @dataProvider providerXoSoNgayThuBay
     */
    
	public function testXoSoNgayThuBay($tinh, $expectedResult)
	{
		$helpers = new Helpers();

	    $ketquaXS = $helpers->getKQXS("http://lechung.net/ket-qua-xo-so-" . $helpers->convert_string_vi($tinh) . "/04-08-2018.html");
		$ketquaXS = json_decode($ketquaXS, true);

		if(isset($ketquaXS["giai_tam"])) {
		    $this->assertEquals($ketquaXS["giai_tam"], $expectedResult["giai_tam"]);
		}

	    $this->assertEquals($ketquaXS["giai_bay"], $expectedResult["giai_bay"]);
	    $this->assertEquals($ketquaXS["giai_sau"], $expectedResult["giai_sau"]);
	    $this->assertEquals($ketquaXS["giai_nam"], $expectedResult["giai_nam"]);
	    $this->assertEquals($ketquaXS["giai_tu"], $expectedResult["giai_tu"]);
	    $this->assertEquals($ketquaXS["giai_ba"], $expectedResult["giai_ba"]);
	    $this->assertEquals($ketquaXS["giai_nhi"], $expectedResult["giai_nhi"]);
	    $this->assertEquals($ketquaXS["giai_nhat"], $expectedResult["giai_nhat"]);
	    $this->assertEquals($ketquaXS["giai_dac_biet"], $expectedResult["giai_dac_biet"]);
	}

	public function providerXoSoNgayThuBay() {
	    return [
	        	[
		        	"Bình Phước", [
		        		"giai_tam" => ["93"],
						"giai_bay" => ["825"],
						"giai_sau" => ["7679","9264","9383"],
						"giai_nam" => ["7981"],
						"giai_tu" => ["15585","75098","09742","26081","56316","45731","03934"],
						"giai_ba" => ["37320","86056"],
						"giai_nhi" => ["25917"],
						"giai_nhat" => ["86746"],
						"giai_dac_biet" => ["849565"]
		        	]
		        ],

	        	[
	        		"Đà Nẵng", [
		        		"giai_tam" => ["48"],
						"giai_bay" => ["606"],
						"giai_sau" => ["2084","8193","8686"],
						"giai_nam" => ["6127"],
						"giai_tu" => ["33112","52363","86118","33967","72767","79861","69068"],
						"giai_ba" => ["39132","45763"],
						"giai_nhi" => ["64795"],
						"giai_nhat" => ["07217"],
						"giai_dac_biet" => ["979812"]
		        	],
	        	],

	        	[
	        		"Đắk Nông", [
	        			"giai_tam" => ["17"],
						"giai_bay" => ["139"],
						"giai_sau" => ["7811","4981","3234"],
						"giai_nam" => ["6130"],
						"giai_tu" => ["01069","70798","42440","97015","50461","35694","83659"],
						"giai_ba" => ["54595","68069"],
						"giai_nhi" => ["96862"],
						"giai_nhat" => ["72637"],
						"giai_dac_biet" => ["936776"]
	        		]
	        	],

	        	[
	        		"Hậu Giang", [
						"giai_tam" => ["65"],
						"giai_bay" => ["259"],
						"giai_sau" => ["8291","3625","2645"],
						"giai_nam" => ["5792"],
						"giai_tu" => ["32561","53239","69931","42355","49246","40303","44136"],
						"giai_ba" => ["86992","73612"],
						"giai_nhi" => ["12379"],
						"giai_nhat" => ["93534"],
						"giai_dac_biet" => ["661333"]
	        		]
	        	],

	        	[
	        		"Long An", [
	        			"giai_tam" => ["67"],
						"giai_bay" => ["713"],
						"giai_sau" => ["1713","3528","8326"],
						"giai_nam" => ["7042"],
						"giai_tu" => ["34010","45497","98812","95932","98988","31990","79987"],
						"giai_ba" => ["01451","28941"],
						"giai_nhi" => ["27476"],
						"giai_nhat" => ["90770"],
						"giai_dac_biet" => ["479236"]
	        		]
	        	],

	        	[
	        		"Quảng Ngãi", [
						"giai_tam" => ["73"],
						"giai_bay" => ["400"],
						"giai_sau" => ["7069","2279","2340"],
						"giai_nam" => ["1355"],
						"giai_tu" => ["83423","13019","34743","70896","06414","16612","93563"],
						"giai_ba" => ["36290","62907"],
						"giai_nhi" => ["05089"],
						"giai_nhat" => ["28979"],
						"giai_dac_biet" => ["624586"]
	        		]
	        	],

	        	[
	        		"TP. HCM", [
						"giai_tam" => ["37"],
						"giai_bay" => ["385"],
						"giai_sau" => ["8829","1857","5964"],
						"giai_nam" => ["1957"],
						"giai_tu" => ["55826","85453","74616","04866","21608","96047","58041"],
						"giai_ba" => ["41411","64108"],
						"giai_nhi" => ["11930"],
						"giai_nhat" => ["69466"],
						"giai_dac_biet" => ["115395"]
	        		]
	        	],

	        	[
	        		"Nam Định", [
						"giai_bay" => ["93","23","86","39"],
						"giai_sau" => ["208","422","183"],
						"giai_nam" => ["7665","1091","6288","0662","8141","7676"],
						"giai_tu" => ["9697","5366","9962","0914"],
						"giai_ba" => ["81674","83035","91802","05501","35672","62985"],
						"giai_nhi" => ["34453","34579"],
						"giai_nhat" => ["96792"],
						"giai_dac_biet" => ["88624"]
	        		]
	        	]
	    ];
	}	
}