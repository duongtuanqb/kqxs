<?php
/**
 * Test Xo so thu sau ngay 03-08-2018
 *   [ "Bình Dương", "Gia Lai", "Ninh Thuận", "Trà Vinh", "Vĩnh Long", "Hải Phòng" ]
 */
namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Helpers;

class ThuSauTests extends TestCase
{
	/**
     * @dataProvider providerXoSoNgayThuSau
     */
    
	public function testXoSoNgayThuSau($tinh, $expectedResult)
	{
		$helpers = new Helpers();

	    $ketquaXS = $helpers->getKQXS("http://lechung.net/ket-qua-xo-so-" . $helpers->convert_string_vi($tinh) . "/03-08-2018.html");
		$ketquaXS = json_decode($ketquaXS, true);

		if(isset($ketquaXS["giai_tam"])) {
		    $this->assertEquals($ketquaXS["giai_tam"], $expectedResult["giai_tam"]);
		}

	    $this->assertEquals($ketquaXS["giai_bay"], $expectedResult["giai_bay"]);
	    $this->assertEquals($ketquaXS["giai_sau"], $expectedResult["giai_sau"]);
	    $this->assertEquals($ketquaXS["giai_nam"], $expectedResult["giai_nam"]);
	    $this->assertEquals($ketquaXS["giai_tu"], $expectedResult["giai_tu"]);
	    $this->assertEquals($ketquaXS["giai_ba"], $expectedResult["giai_ba"]);
	    $this->assertEquals($ketquaXS["giai_nhi"], $expectedResult["giai_nhi"]);
	    $this->assertEquals($ketquaXS["giai_nhat"], $expectedResult["giai_nhat"]);
	    $this->assertEquals($ketquaXS["giai_dac_biet"], $expectedResult["giai_dac_biet"]);
	}

	public function providerXoSoNgayThuSau() {
	    return [
	        	[
		        	"Bình Dương",  [
		        		"giai_tam" => ["38"],
						"giai_bay" => ["870"],
						"giai_sau" => ["4277","4217","2342"],
						"giai_nam" => ["4163"],
						"giai_tu" => ["07255","13105","04003","87556","88935","87578","15601"],
						"giai_ba" => ["15805","59576"],
						"giai_nhi" => ["26685"],
						"giai_nhat" => ["28244"],
						"giai_dac_biet" => ["866710"]
		        	]
		        ],

	        	[
	        		"Gia Lai",   [
		        		"giai_tam" => ["97"],
						"giai_bay" => ["168"],
						"giai_sau" => ["0902","7458","2672"],
						"giai_nam" => ["6612"],
						"giai_tu" => ["99379","77745","93152","41601","88438","78660","51252"],
						"giai_ba" => ["02390","97796"],
						"giai_nhi" => ["92946"],
						"giai_nhat" => ["26236"],
						"giai_dac_biet" => ["894092"]
		        	],
	        	],

	        	[
	        		"Ninh Thuận",  [
	        			"giai_tam" => ["40"],
						"giai_bay" => ["749"],
						"giai_sau" => ["7253","3911","9843"],
						"giai_nam" => ["3121"],
						"giai_tu" => ["21935","46253","50240","24848","21850","59526","56053"],
						"giai_ba" => ["16629","87517"],
						"giai_nhi" => ["35023"],
						"giai_nhat" => ["31718"],
						"giai_dac_biet" => ["713090"]
	        		]
	        	],

	        	[
	        		"Trà Vinh", [
						"giai_tam" => ["95"],
						"giai_bay" => ["059"],
						"giai_sau" => ["3783","2403","7214"],
						"giai_nam" => ["5885"],
						"giai_tu" => ["33927","03540","88683","13884","31795","53540","50695"],
						"giai_ba" => ["42333","23153"],
						"giai_nhi" => ["18431"],
						"giai_nhat" => ["17518"],
						"giai_dac_biet" => ["459985"]
	        		]
	        	],

	        	[
	        		"Vĩnh Long", [
	        			"giai_tam" => ["61"],
						"giai_bay" => ["809"],
						"giai_sau" => ["8666","5831","3743"],
						"giai_nam" => ["3577"],
						"giai_tu" => ["08324","51081","12546","19959","73650","57127","74934"],
						"giai_ba" => ["51453","57480"],
						"giai_nhi" => ["14228"],
						"giai_nhat" => ["75906"],
						"giai_dac_biet" => ["748827"]
	        		]
	        	],

	        	[
	        		"Hải Phòng", [
						"giai_bay" => ["64","33","48","80"],
						"giai_sau" => ["215","545","972"],
						"giai_nam" => ["8183","5279","6416","2292","2970","6392"],
						"giai_tu" => ["6813","6192","8050","5544"],
						"giai_ba" => ["49449","40401","56566","55192","18920","05414"],
						"giai_nhi" => ["22436","09007"],
						"giai_nhat" => ["41963"],
						"giai_dac_biet" => ["49786"]
	        		]
	        	]
	    ];
	}	
}