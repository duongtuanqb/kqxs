<?php
/**
 * Test Xo so thu hai ngay 30-07-2018
 * [ "Cà Mau","Đồng Tháp","Phú Yên","Thừa Thiên Huế","TP. HCM","Hà Nội" ]
 */
namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Helpers;

class ThuHaiTests extends TestCase
{
	/**
     * @dataProvider providerXoSoNgayThuHai
     */
    
	public function testXoSoNgayThuHai($tinh, $expectedResult)
	{
		$helpers = new Helpers();

	    $ketquaXS = $helpers->getKQXS("http://lechung.net/ket-qua-xo-so-" . $helpers->convert_string_vi($tinh) . "/30-07-2018.html");
		$ketquaXS = json_decode($ketquaXS, true);

		if(isset($ketquaXS["giai_tam"])) {
		    $this->assertEquals($ketquaXS["giai_tam"], $expectedResult["giai_tam"]);
		}

	    $this->assertEquals($ketquaXS["giai_bay"], $expectedResult["giai_bay"]);
	    $this->assertEquals($ketquaXS["giai_sau"], $expectedResult["giai_sau"]);
	    $this->assertEquals($ketquaXS["giai_nam"], $expectedResult["giai_nam"]);
	    $this->assertEquals($ketquaXS["giai_tu"], $expectedResult["giai_tu"]);
	    $this->assertEquals($ketquaXS["giai_ba"], $expectedResult["giai_ba"]);
	    $this->assertEquals($ketquaXS["giai_nhi"], $expectedResult["giai_nhi"]);
	    $this->assertEquals($ketquaXS["giai_nhat"], $expectedResult["giai_nhat"]);
	    $this->assertEquals($ketquaXS["giai_dac_biet"], $expectedResult["giai_dac_biet"]);
	}

	public function providerXoSoNgayThuHai() {
	    return [
	        	[
		        	"Cà Mau", [
		        		"giai_tam" => ["97"],
						"giai_bay" => ["842"],
						"giai_sau" => ["4432","9629","2139"],
						"giai_nam" => ["2131"],
						"giai_tu" => ["67641","96132","44619","58939","12049","45359","29727"],
						"giai_ba" => ["24757","94601"],
						"giai_nhi" => ["24313"],
						"giai_nhat" => ["36157"],
						"giai_dac_biet" => ["207640"]
		        	]
		        ],

	        	[
	        		"Đồng Tháp", [
		        		"giai_tam" => ["90"],
						"giai_bay" => ["388"],
						"giai_sau" => ["1202","9727","0834"],
						"giai_nam" => ["1985"],
						"giai_tu" => ["04002","15226","23721","34060","16876","67057","92264"],
						"giai_ba" => ["24224","63652"],
						"giai_nhi" => ["03700"],
						"giai_nhat" => ["01068"],
						"giai_dac_biet" => ["517112"]
		        	],
	        	],

	        	[
	        		"Phú Yên", [
	        			"giai_tam" => ["60"],
						"giai_bay" => ["508"],
						"giai_sau" => ["4871","2213","0401"],
						"giai_nam" => ["0785"],
						"giai_tu" => ["55555","37539","99626","93176","97466","93771","06840"],
						"giai_ba" => ["27435","78695"],
						"giai_nhi" => ["93169"],
						"giai_nhat" => ["46450"],
						"giai_dac_biet" => ["406822"]
	        		]
	        	],

	        	[
	        		"Thừa Thiên Huế", [
	        			"giai_tam" => ["47"],
						"giai_bay" => ["147"],
						"giai_sau" => ["7566","7216","7471"],
						"giai_nam" => ["4855"],
						"giai_tu" => ["11955","08030","69856","80347","85190","45825","27987"],
						"giai_ba" => ["70213","62863"],
						"giai_nhi" => ["27794"],
						"giai_nhat" => ["62905"],
						"giai_dac_biet" => ["269191"]
	        		]
	        	],

	        	[
	        		"TP. HCM", [
	        			"giai_tam" => ["41"],
						"giai_bay" => ["451"],
						"giai_sau" => ["4965","6770","3456"],
						"giai_nam" => ["9075"],
						"giai_tu" => ["95452","79877","00707","53852","33179","69169","90255"],
						"giai_ba" => ["27749","75085"],
						"giai_nhi" => ["80745"],
						"giai_nhat" => ["80935"],
						"giai_dac_biet" => ["060340"]
	        		]
	        	],

	        	[
	        		"Hà Nội", [
						"giai_bay" => ["81","43","44","90"],
						"giai_sau" => ["624","751","598"],
						"giai_nam" => ["2487","8885","4083","7838","2546","7922"],
						"giai_tu" => ["5692","1697","7254","2443"],
						"giai_ba" => ["66006","50641","61297","48080","80822","22435"],
						"giai_nhi" => ["78784","92305"],
						"giai_nhat" => ["28906"],
						"giai_dac_biet" => ["19672"]
	        		]
	        	]
	    ];
	}	
}