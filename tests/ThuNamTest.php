<?php
/**
 * Test Xo so thu nam ngay 02-08-2018
 * [ "An Giang", "Bình Định", "Bình Thuận", "Quảng Bình", "Quảng Trị", "Tây Ninh", "Hà Nội" ]
 */
namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Helpers;

class ThuNamTests extends TestCase
{
	/**
     * @dataProvider providerXoSoNgayThuNam
     */
    
	public function testXoSoNgayThuNam($tinh, $expectedResult)
	{
		$helpers = new Helpers();

	    $ketquaXS = $helpers->getKQXS("http://lechung.net/ket-qua-xo-so-" . $helpers->convert_string_vi($tinh) . "/02-08-2018.html");
		$ketquaXS = json_decode($ketquaXS, true);

		if(isset($ketquaXS["giai_tam"])) {
		    $this->assertEquals($ketquaXS["giai_tam"], $expectedResult["giai_tam"]);
		}

	    $this->assertEquals($ketquaXS["giai_bay"], $expectedResult["giai_bay"]);
	    $this->assertEquals($ketquaXS["giai_sau"], $expectedResult["giai_sau"]);
	    $this->assertEquals($ketquaXS["giai_nam"], $expectedResult["giai_nam"]);
	    $this->assertEquals($ketquaXS["giai_tu"], $expectedResult["giai_tu"]);
	    $this->assertEquals($ketquaXS["giai_ba"], $expectedResult["giai_ba"]);
	    $this->assertEquals($ketquaXS["giai_nhi"], $expectedResult["giai_nhi"]);
	    $this->assertEquals($ketquaXS["giai_nhat"], $expectedResult["giai_nhat"]);
	    $this->assertEquals($ketquaXS["giai_dac_biet"], $expectedResult["giai_dac_biet"]);
	}

	public function providerXoSoNgayThuNam() {
	    return [
	        	[
		        	"An Giang",  [
		        		"giai_tam" => ["62"],
						"giai_bay" => ["302"],
						"giai_sau" => ["4042","0101","7592"],
						"giai_nam" => ["1278"],
						"giai_tu" => ["23998","05181","27171","34825","74504","70581","92932"],
						"giai_ba" => ["39233","02925"],
						"giai_nhi" => ["82208"],
						"giai_nhat" => ["78980"],
						"giai_dac_biet" => ["850482"]
		        	]
		        ],

	        	[
	        		"Bình Định",  [
		        		"giai_tam" => ["59"],
						"giai_bay" => ["493"],
						"giai_sau" => ["4667","2011","6054"],
						"giai_nam" => ["0511"],
						"giai_tu" => ["41547","69722","39839","55209","29314","84134","83255"],
						"giai_ba" => ["13238","86283"],
						"giai_nhi" => ["78658"],
						"giai_nhat" => ["86470"],
						"giai_dac_biet" => ["868160"]
		        	],
	        	],

	        	[
	        		"Bình Thuận",  [
	        			"giai_tam" => ["04"],
						"giai_bay" => ["179"],
						"giai_sau" => ["6899","1824","9469"],
						"giai_nam" => ["9527"],
						"giai_tu" => ["61960","18461","19441","01665","64669","55031","26986"],
						"giai_ba" => ["03213","85956"],
						"giai_nhi" => ["93368"],
						"giai_nhat" => ["73181"],
						"giai_dac_biet" => ["955959"]
	        		]
	        	],

	        	[
	        		"Quảng Bình",  [
						"giai_tam" => ["24"],
						"giai_bay" => ["688"],
						"giai_sau" => ["1739","9050","1961"],
						"giai_nam" => ["2951"],
						"giai_tu" => ["73480","68388","55243","06836","93054","03858","60964"],
						"giai_ba" => ["70914","10390"],
						"giai_nhi" => ["42584"],
						"giai_nhat" => ["20898"],
						"giai_dac_biet" => ["771547"]
	        		]
	        	],

	        	[
	        		"Quảng Trị",  [
	        			"giai_tam" => ["43"],
						"giai_bay" => ["819"],
						"giai_sau" => ["5186","0284","3756"],
						"giai_nam" => ["4617"],
						"giai_tu" => ["66696","32867","31854","81532","85142","13753","05418"],
						"giai_ba" => ["46538","87519"],
						"giai_nhi" => ["38211"],
						"giai_nhat" => ["71786"],
						"giai_dac_biet" => ["272760"]
	        		]
	        	],

	        	[
	        		"Tây Ninh", [
						"giai_tam" => ["84"],
						"giai_bay" => ["975"],
						"giai_sau" => ["7350","8299","1002"],
						"giai_nam" => ["3935"],
						"giai_tu" => ["41571","93590","48666","72697","63547","61310","92207"],
						"giai_ba" => ["56745","40839"],
						"giai_nhi" => ["95557"],
						"giai_nhat" => ["61634"],
						"giai_dac_biet" => ["626390"]
	        		]
	        	],

	        	[
	        		"Hà Nội", [
						"giai_bay" => ["34","67","20","89"],
						"giai_sau" => ["644","640","786"],
						"giai_nam" => ["6267","1090","0769","3468","6508","6544"],
						"giai_tu" => ["4173","7897","1656","6186"],
						"giai_ba" => ["69163","75122","23688","80399","19096","23834"],
						"giai_nhi" => ["86786","74188"],
						"giai_nhat" => ["36202"],
						"giai_dac_biet" => ["25638"]
	        		]
	        	]
	    ];
	}	
}