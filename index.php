<?php
namespace App;

require_once __DIR__.'/vendor/autoload.php';

use App\Helpers;
use Dotenv\Dotenv;
$dotenv  =  new Dotenv(__DIR__); 
$dotenv->load();

date_default_timezone_set("Asia/Ho_Chi_Minh");

$helpers = new Helpers();

// validate
if(!isset($_GET["pass"]) || ($_GET["pass"] != $_ENV["PASS"])) {
    echo $helpers->json_response("Sai password", 400);
    die;
}

if(!isset($_GET["tinh"]) || !isset($_GET["ngay"])) {
    echo $helpers->json_response("Thiếu thông tin", 422);
    die;
}

if(!($d = \DateTime::createFromFormat("d-m-Y", $_GET["ngay"])) || ($d->format($_GET["ngay"]) !== $_GET["ngay"])) {

    echo $helpers->json_response("Ngày không đúng định dạng d-m-Y", 400);
    die;
}

if(!$helpers->checkLich($_GET["tinh"], $_GET["ngay"])) {
    echo $helpers->json_response("Không có kết quả", 404);
    die;
}
// end validate

// run
header('Content-Type: application/json');
print_r($helpers->getKQXS("http://lechung.net/ket-qua-xo-so-" . $helpers->convert_string_vi($_GET["tinh"]) . "/" . $_GET["ngay"] . ".html"));